package com.smartgeosystem.geoserverconfigurator.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smartgeosystem.geoserverconfigurator.config.GeoserverConnectionProperties;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

// TODO: move to RestTemplate!
// TODO: need refactoring!

@Slf4j
@Service
public class GeoserverClientService {
    final private GeoserverConnectionProperties connProperties;
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final MediaType XML = MediaType.parse("application/xml; charset=utf-8");
    private OkHttpClient client;

    public GeoserverClientService(GeoserverConnectionProperties connProperties) {
        this.connProperties = connProperties;

        this.client = new OkHttpClient.Builder()
                .connectTimeout(90, TimeUnit.SECONDS)
                .writeTimeout(90, TimeUnit.SECONDS)
                .readTimeout(90, TimeUnit.SECONDS)
                .authenticator(
                    new Authenticator() {
                        @Override
                        public Request authenticate(Route route, Response response) throws IOException {
                            String credentials = Credentials.basic(connProperties.getUser(), connProperties.getPassword());
                            return response.request().newBuilder().header("Authorization", credentials).build();
                        }
                    }
                ).build();
    }


    // Common
    private String _constructSubUrl(String subUrl) {
        return this.connProperties.getBaseAddress() + subUrl;
    }

    private boolean _getAndCheckCode(String fullUrl) throws IOException {
        Request request = new Request.Builder()
                .url(fullUrl)
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (response.code() >= 400)
                return false;
        }
        catch (IOException e) {
            log.error("Oops", e);
            throw e;
        }
        return true;
    }

    private boolean _headAndCheckCode(String fullUrl) throws IOException {
        Request request = new Request.Builder()
                .url(fullUrl)
                .head()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (response.code() >= 400)
                return false;
        }
        catch (IOException e) {
            log.error("Oops", e);
            throw e;
        }
        return true;
    }

    private boolean _uploadPutAndCheckCode(String dataFileUrl, File localPath) throws IOException {
        String mimeType = Files.probeContentType(localPath.toPath());
        return this._uploadPutAndCheckCode(dataFileUrl, localPath, mimeType);
    }

    private boolean _uploadPutAndCheckCode(String dataFileUrl, File localPath, String mimeType) throws IOException {
        if(mimeType==null || mimeType.isEmpty())
            mimeType = Files.probeContentType(localPath.toPath());
        RequestBody body = RequestBody.create(MediaType.parse(mimeType), localPath);

        HttpUrl httpUrl = HttpUrl.parse(dataFileUrl);
        Request request = new Request.Builder()
                .url(httpUrl)
                .put(body)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (response.code() >= 400)
                return false;
        }
        catch (IOException e) {
            log.error("Oops", e);
            throw e;
        }
        return true;
    }

    private boolean _uploadPostAndCheckCode(String dataFileUrl, File localPath) throws IOException {
        String mimeType = Files.probeContentType(localPath.toPath());
        return this._uploadPostAndCheckCode(dataFileUrl, localPath, mimeType);
    }

    private boolean _uploadPostAndCheckCode(String dataFileUrl, File localPath, String mimeType) throws IOException {
        if(mimeType==null || mimeType.isEmpty())
            mimeType = Files.probeContentType(localPath.toPath());
        RequestBody body = RequestBody.create(MediaType.parse(mimeType), localPath);

        HttpUrl httpUrl = HttpUrl.parse(dataFileUrl);
        Request request = new Request.Builder()
                .url(httpUrl)
                .post(body)
                .build();

        try (Response response = client.newCall(request).execute()) {
            if (response.code() >= 400)
                return false;
        }
        catch (IOException e) {
            log.error("Oops", e);
            throw e;
        }
        return true;
    }


    private HashMap<String, Object> _getContent(String fullUrl) throws IOException {
        Request request = new Request.Builder()
                .url(fullUrl)
                .build();

        try (Response response = client.newCall(request).execute()) {
            ObjectMapper mapper = new ObjectMapper();
            HashMap<String, Object> map = new HashMap<String, Object>();
            map = mapper.readValue(response.body().string(), new TypeReference<HashMap<String, Object>>(){});
            return map;
        }
        catch (IOException e) {
            log.error("Oops", e);
            throw e;
        }
    }

    private String _getRawContent(String fullUrl) throws IOException {
        Request request = new Request.Builder()
                .url(fullUrl)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
        catch (IOException e) {
            log.error("Oops", e);
            throw e;
        }
    }


    private boolean _postAndCheckCode(String fullUrl, String jsonContent) throws IOException {
        RequestBody body = RequestBody.create(this.JSON, jsonContent);
        Request request = new Request.Builder()
            .url(fullUrl)
            .post(body)
            .build();

        try (Response response = client.newCall(request).execute()) {
            if (response.code() >= 400)
                return false;
        }
        catch (IOException e) {
            log.error("Oops", e);
            throw e;
        }
        return true;
    }

    private boolean _putAndCheckCode(String fullUrl, String xmlContent) throws IOException {
        RequestBody body = RequestBody.create(this.XML, xmlContent);
        HttpUrl httpUrl = HttpUrl.parse(fullUrl);
        Request request = new Request.Builder()
            .url(httpUrl)
            .put(body)
            .build();

        try (Response response = client.newCall(request).execute()) {
            if (response.code() >= 400)
                return false;
        }
        catch (IOException e) {
            log.error("Oops", e);
            throw e;
        }
        return true;
    }


    public boolean checkStatus() {
        String rootUrl = this._constructSubUrl("/web");
        try {
            return this._getAndCheckCode(rootUrl);
        } catch (IOException e) {
            return false;
        }
    }


    // Workspace
    public boolean workspaceExists(String workspaceName) throws Exception {
        String workspacesUrl = this._constructSubUrl("/rest/workspaces");
        //check existing
        HashMap<String, Object> workspaces = this._getContent(workspacesUrl);
        if(workspaces.containsKey("workspaces") && (workspaces.get("workspaces") instanceof HashMap)) {
            HashMap<String, Object> workspacesList = (HashMap<String, Object>) workspaces.get("workspaces");
            if(!workspacesList.isEmpty() && workspacesList.containsKey("workspace")) {
                ArrayList<HashMap> workspaceArray = (ArrayList<HashMap>) workspacesList.get("workspace");
                for (HashMap result : workspaceArray) {
                    if(result.containsKey("name") && result.get("name").equals(workspaceName))
                        return true;
                }
            }
        }
        return false;
    }

    public boolean createWorkspace(String workspaceName) throws Exception {
        //check existing
        if(this.workspaceExists(workspaceName))
            return false;
        //add new
        String workspacesUrl = this._constructSubUrl("/rest/workspaces");
        String payload = String.format("{'workspace': {'name': %s}}", workspaceName);
        if(!this._postAndCheckCode(workspacesUrl, payload))
            throw new Exception(String.format("Error on create new workspace '%s'", workspaceName));
        return true;
    }


    // Datasource
    public boolean dataSourceExists(String workspaceName, String dataSourceName) throws IOException {
        String datastoresUrl = this._constructSubUrl(String.format("/rest/workspaces/%s/datastores", workspaceName));
        //check existing
        HashMap<String, Object> datastores = this._getContent(datastoresUrl);
        if(datastores.containsKey("dataStores") && (datastores.get("dataStores") instanceof HashMap)) {
            HashMap<String, Object> workspacesList = (HashMap<String, Object>) datastores.get("dataStores");
            if(!workspacesList.isEmpty() && workspacesList.containsKey("dataStore")) {
                ArrayList<HashMap> workspaceArray = (ArrayList<HashMap>) workspacesList.get("dataStore");
                for (HashMap result : workspaceArray) {
                    if(result.containsKey("name") && result.get("name").equals(dataSourceName))
                        return true;
                }
            }
        }
        return false;
    }


    public boolean appendPgDataSource(String workspaceName,
                                    String dataSourceName,
                                    String dbHost,
                                    int dbPort,
                                    String dbName,
                                    String dbSchema,
                                    String dbUser,
                                    String dbPass) throws Exception {
        //check existing
        if(this.dataSourceExists(workspaceName, dataSourceName))
            return false;
        //add new
        String datastoresUrl = this._constructSubUrl(String.format("/rest/workspaces/%s/datastores", workspaceName));
        String payload = String.format("{'dataStore': {" +
                "    'name': '%s'," +
                "    'connectionParameters': {" +
                "      'entry': [" +
                "        {'@key':'host','$':'%s'}," +
                "        {'@key':'port','$':'%s'}," +
                "        {'@key':'database','$':'%s'}," +
                "        {'@key':'user','$':'%s'}," +
                "        {'@key':'passwd','$':'%s'}," +
                "        {'@key':'schema','$':'%s'}," +
                "        {'@key':'dbtype','$':'postgis'}" +
                "]}}}", dataSourceName, dbHost, dbPort, dbName, dbUser, dbPass, dbSchema);

        if(!this._postAndCheckCode(datastoresUrl, payload))
            throw new Exception(String.format("Error on create new datasource '%s'", dataSourceName));
        return true;
    }


    public boolean appendGpkgDataSource(String workspaceName,
                                      String dataSourceName,
                                      String gpkgPath) throws Exception {
        //check existing
        if(this.dataSourceExists(workspaceName, dataSourceName))
            return false;

        //add new
        String datastoresUrl = this._constructSubUrl(String.format("/rest/workspaces/%s/datastores", workspaceName));
        String payload = String.format("{'dataStore': {" +
                "    'name': '%s'," +
                "    'connectionParameters': {" +
                "      'entry': [" +
                "        {'@key':'database','$':'%s'}," +
                "        {'@key':'dbtype','$':'geopkg'}" +
                "]}}}", dataSourceName, gpkgPath);

        if(!this._postAndCheckCode(datastoresUrl, payload))
            throw new Exception(String.format("Error on create new datasource '%s'", dataSourceName));
        return true;
    }


    // Publish layers
    public List<String> getUnpublishedLayers(String workspaceName, String dataSourceName) throws IOException {
        List<String> unpublishedLayers = new ArrayList<String>();

        String availableFeaturetypesUrl = this._constructSubUrl(String.format(
                "/rest/workspaces/%s/datastores/%s/featuretypes.json?list=available",
                workspaceName, dataSourceName)
        );
        HashMap<String, Object> featuretypes = this._getContent(availableFeaturetypesUrl);

        if(featuretypes.containsKey("list") && (featuretypes.get("list") instanceof HashMap)) {
            HashMap<String, Object> featuretypesList = (HashMap<String, Object>) featuretypes.get("list");
            if(!featuretypesList.isEmpty() && featuretypesList.containsKey("string")) {
                unpublishedLayers = (ArrayList<String>) featuretypesList.get("string");
            }
        }
        return unpublishedLayers;
    }

    public List<String> getPublishedLayers(String workspaceName, String dataSourceName) throws IOException {
        List<String> publishedLayersNames = new ArrayList<String>();

        String featuretypesUrl = this._constructSubUrl(String.format(
                "/rest/workspaces/%s/datastores/%s/featuretypes.json",
                workspaceName, dataSourceName)
        );
        HashMap<String, Object> featuretypes = this._getContent(featuretypesUrl);

        if(featuretypes.containsKey("featureTypes") && (featuretypes.get("featureTypes") instanceof HashMap)) {
            HashMap<String, Object> featuretypesList = (HashMap<String, Object>) featuretypes.get("featureTypes");
            if(!featuretypesList.isEmpty() && featuretypesList.containsKey("featureType")) {
                List<HashMap<String, Object>> publishedLayers = (ArrayList<HashMap<String, Object>>) featuretypesList.get("featureType");
                for(HashMap<String, Object> layer: publishedLayers) {
                    if(!layer.isEmpty() && layer.containsKey("name")) {
                        publishedLayersNames.add((String)layer.get("name"));
                    }
                }
            }
        }
        return publishedLayersNames;
    }

    public boolean publishLayer(String workspaceName, String dataSourceName, String layerName) throws Exception {
        String featuretypesUrl = this._constructSubUrl(String.format(
                "/rest/workspaces/%s/datastores/%s/featuretypes",
                workspaceName, dataSourceName)
        );
        String payload = String.format("{'featureType': {" +
                "'name': '%s', " +
                "'nativeName': '%s', " +
                "'title': '%s', " +
                "'projectionPolicy': 'NONE'," +
                " 'enabled': 'true', " +
                "'nativeCRS': 'EPSG:4326'" +
                "}}", layerName, layerName, layerName);  //TODO: WTF!
        if(!this._postAndCheckCode(featuretypesUrl, payload))
            throw new Exception(String.format("Error on publish layer '%s'", layerName));
        return true;
    }

    public boolean setLayerStyle(String workspaceName, String layerName, String styleName) throws Exception {

        String layerUrl = this._constructSubUrl(String.format(
                "/rest/layers/%s:%s",
                workspaceName, layerName)
        );

        String payload = String.format("<layer><defaultStyle><name>%s</name></defaultStyle></layer>", styleName);
        if(!this._putAndCheckCode(layerUrl, payload))
            throw new Exception(String.format("Error on set style for layer '%s'", layerName));
        return true;
    }


    public boolean recalculateLayerExtent(String workspaceName, String dataSourceName, String layerName) throws Exception {
        String featuretypesUrl = this._constructSubUrl(String.format(
                "/rest/workspaces/%s/datastores/%s/featuretypes/%s.xml?recalculate=nativebbox,latlonbbox",
                workspaceName, dataSourceName, layerName)
        );
        String content = String.format("<featureType><name>%s</name><enabled>true</enabled></featureType>", layerName);
        if(!this._putAndCheckCode(featuretypesUrl, content))
            throw new Exception(String.format("Error on recalculate extent for layer '%s'", layerName));
        return true;
    }

    public boolean publishView(String workspaceName, String dataSourceName, String content) throws Exception {
        String featuretypesUrl = this._constructSubUrl(String.format(
                "/rest/workspaces/%s/datastores/%s/featuretypes",
                workspaceName, dataSourceName)
        );
        if(!this._postAndCheckCode(featuretypesUrl, content))
            throw new Exception("Error on publish view!");
        return true;
    }


    // Settings
    public boolean setProxySettings(String proxyAddress) throws Exception {
        String featuretypesUrl = this._constructSubUrl("/rest/settings.xml");
        String content = String.format("<global><settings><proxyBaseUrl>%s</proxyBaseUrl></settings><useHeadersProxyURL>true</useHeadersProxyURL></global>", proxyAddress);
        if(!this._putAndCheckCode(featuretypesUrl, content))
            throw new Exception(String.format("Error on set proxy address '%s'", proxyAddress));
        return true;
    }

    public boolean setEncoding(String encoding) throws Exception {
        String featuretypesUrl = this._constructSubUrl("/rest/settings.xml");
        String content = String.format("<global><settings><charset>%s</charset></settings></global>", encoding);
        if(!this._putAndCheckCode(featuretypesUrl, content))
            throw new Exception(String.format("Error on set encoding '%s'", encoding));
        return true;
    }

    public boolean setNumDecimals(String numDecimals) throws Exception {
        String featuretypesUrl = this._constructSubUrl("/rest/settings.xml");
        String content = String.format("<global><settings><numDecimals>%s</numDecimals></settings></global>", numDecimals);
        if(!this._putAndCheckCode(featuretypesUrl, content))
            throw new Exception(String.format("Error on set numDecimals '%s'", numDecimals));
        return true;
    }

    public boolean setWmsSrs(String srs) throws Exception {
        String featuretypesUrl = this._constructSubUrl("/rest/services/wms/settings.xml");
        String content = String.format("<wms><srs><string>%s</string></srs></wms>", srs);
        if(!this._putAndCheckCode(featuretypesUrl, content))
            throw new Exception("Error on set WMS srs!");
        return true;
    }

    public boolean setGlobalSettings(String proxyAddress, String encoding, String numDecimals) throws Exception {
        String featuretypesUrl = this._constructSubUrl("/rest/settings.xml");
        String content = String.format(
                "<global><settings><proxyBaseUrl>%s</proxyBaseUrl><charset>%s</charset><numDecimals>%s</numDecimals></settings><useHeadersProxyURL>true</useHeadersProxyURL></global>",
                proxyAddress,
                encoding,
                numDecimals
        );
        if(!this._putAndCheckCode(featuretypesUrl, content))
            throw new Exception("Error on set global settings!");
        return true;
    }

    // DataFiles
    public boolean dataFileExists(String remotePath) throws IOException {
        String dataFileUrl = this._constructSubUrl("/rest/resource/data/" + remotePath);
        return this._headAndCheckCode(dataFileUrl);
    }


    public boolean uploadDataFile(File localPath, String remotePath) throws IOException {
        String dataFileUrl = this._constructSubUrl("/rest/resource/data/" + remotePath);
        return this._uploadPutAndCheckCode(dataFileUrl, localPath);
    }

    // Styles
    public boolean styleExists(String workspaceName, String styleName) throws IOException {
        String styleUrl = this._constructSubUrl(String.format("/rest/workspaces/%s/styles/%s.json", workspaceName, styleName));
        return this._getAndCheckCode(styleUrl);
    }

    public boolean uploadStyle(String workspaceName, String styleName, File localPath, String mimeType) throws IOException {
        String styleUrl = this._constructSubUrl(String.format("/rest/workspaces/%s/styles", workspaceName, styleName));
        return this._uploadPostAndCheckCode(styleUrl, localPath, mimeType);
    }
}
