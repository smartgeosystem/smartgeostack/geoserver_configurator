package com.smartgeosystem.geoserverconfigurator.service;

import com.smartgeosystem.geoserverconfigurator.config.ConfiguratorProperties;
import com.smartgeosystem.geoserverconfigurator.config.GeoserverConfigurationProperties;
import com.smartgeosystem.geoserverconfigurator.domain.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Slf4j
@Service
public class GeoserverConfiguratorService {

    final private static  Map<StyleType, String> styleMimeTypes;
    static {
        Map<StyleType, String> tempMap = new HashMap<StyleType, String>();
        tempMap.put(StyleType.SLD_1_0, "application/vnd.ogc.sld+xml");
        tempMap.put(StyleType.SLD_1_1, "application/vnd.ogc.se+xml");
        tempMap.put(StyleType.SLD_ZIP, "application/zip");
        tempMap.put(StyleType.CSS, "text/css");

        styleMimeTypes = Collections.unmodifiableMap(tempMap);
    }


    final private ConfiguratorProperties configuratorProperties;
    final private GeoserverConfigurationProperties configuration;
    final private GeoserverClientService geoserverClient;

    public GeoserverConfiguratorService(ConfiguratorProperties config, GeoserverConfigurationProperties configuration, GeoserverClientService client) {
        this.configuratorProperties = config;
        this.configuration = configuration;
        this.geoserverClient = client;
    }


    public boolean waitGeoserver() throws InterruptedException {
        if(configuratorProperties.isWaitGeoserver()) {
            int timeout = configuratorProperties.getWaitTimeout();
            int period = configuratorProperties.getWaitPeriod();
            int current = 0;
            while(current<timeout) {
                if (geoserverClient.checkStatus())
                    break;
                Thread.sleep(period* 1000);
                current += period;
            }
            return current<timeout;
        }
        return true;
    }

    public boolean uploadDataFiles() throws Exception {
        if (configuration.getDataFiles() == null) {
            log.info("\tNo upload configuration. Pass...");
            return true;
        }
        for(DataFileHelper dataFile: configuration.getDataFiles()) {
            // Get source file
            File localPath = new File(dataFile.getLocalPath());
            if (!localPath.exists()) {
                log.error("\tLocal file '{}' does not exists. Pass...", dataFile.getLocalPath());
                continue;
            }
            if (!localPath.isFile()) {
                log.error("\tPath '{}' is not file. Pass...", dataFile.getLocalPath());
                continue;
            }
            // Check file on server exists
            if (this.geoserverClient.dataFileExists(dataFile.getRemotePath())) {
                log.info("\tRemote file '{}' already exists. Pass...", dataFile.getRemotePath());
                continue;
            }
            // Try to upload
            log.info("\tRemote file '{}' does not exists. Upload...", dataFile.getRemotePath());
            this.geoserverClient.uploadDataFile(localPath, dataFile.getRemotePath());
            log.info("\tOK");
        }
        return true;
    }

    public boolean configureWorkspaces() throws Exception {
        if (configuration.getWorkspaces() == null) {
            log.info("\tNo workspace configuration. Pass...");
            return true;
        }
        for (Workspace w: configuration.getWorkspaces()) {
            if(this.geoserverClient.workspaceExists(w.getName())) {
                log.info("\tWorkspace '{}' exists. Pass...", w.getName());
            }
            else {
                log.info("\tWorkspace '{}' does not exists. Creating...", w.getName());
                this.geoserverClient.createWorkspace(w.getName());
                log.info("\tOK");
            }
        }
        return true;
    }

    public boolean configureDataStores() throws Exception {
        if (configuration.getWorkspaces() == null) {
            log.info("\tNo workspace configuration. Pass...");
            return true;
        }
        // Get from workspaces
        for (Workspace w : configuration.getWorkspaces()) {
            for (DataStore ds : w.getDataStores()) {
                if (this.geoserverClient.dataSourceExists(w.getName(), ds.getName())) {
                    log.info("\tDatastore '{}.{}' exists. Pass...", w.getName(), ds.getName());
                } else {
                    log.info("\tDatastore '{}.{}' does not exists. Creating...", w.getName(), ds.getName());
                    if (ds.getType() == DataStoreType.PG) {
                        this.geoserverClient.appendPgDataSource(w.getName(), ds.getName(), ds.getHost(), ds.getPort(), ds.getDb(), ds.getSchema(), ds.getUser(), ds.getPasswd());
                    }
                    if (ds.getType() == DataStoreType.GPKG) {
                        String fullPath = "file:data/" + ds.getPath();
                        this.geoserverClient.appendGpkgDataSource(w.getName(), ds.getName(), fullPath);
                    }
                    log.info("OK");
                }
                if (!ds.getPublishLayers().isEmpty()) {
                    // TODO: Now only '*' value supported
                    ArrayList<String> layersForPublish = ds.getPublishLayers();
                    if(layersForPublish.size() == 1 && layersForPublish.get(0).equals("*")) {
                        List<String> layers = this.geoserverClient.getUnpublishedLayers(w.getName(), ds.getName());

                        if (layers.isEmpty()) {
                            log.info("\t\tAll layers in datastore '{}' already is published", ds.getName());
                        }
                        else {
                            log.info("\t\tAlready published layers for {}:", ds.getName());
                            log.info("\t\t" + String.valueOf(this.geoserverClient.getPublishedLayers(w.getName(), ds.getName())));
                            log.info("\t\tPublish layers for {}:", ds.getName());
                            log.info("\t\t" + String.valueOf(layers));
                            for(String layerName : layers) {
                                this.geoserverClient.publishLayer(w.getName(), ds.getName(), layerName);
                                if(ds.isApplySameNameStyles()) {
                                    log.info("\t\tTry to apply same name style for '{}'...", layerName);
                                    boolean ret = this.geoserverClient.setLayerStyle(w.getName(), layerName, String.format("%s:%s", w.getName(), layerName));
                                    log.info(ret ? "\t\tOK" : "\t\tFail");
                                }
                            }
                        }
                    }
                    else {
                        log.warn("\t\tUnsupported format for list of publishing layers. Only '*' support now");
                    }
                }
            }
        }
        return true;
    }

    public boolean configureStyles() throws Exception {
        if (configuration.getWorkspaces() == null) {
            log.info("\tNo workspace configuration. Pass...");
            return true;
        }
        // Get from workspaces
        for (Workspace w : configuration.getWorkspaces()) {
            for (StyleHelper s : w.getStyles()) {

                if(s.getLocalPath()==null || s.getLocalPath().isEmpty()) {
                    log.error("\tConfiuration error: Need to set local path for style!");
                    continue;
                }
                File localPath = new File(s.getLocalPath());
                if (!localPath.exists()) {
                    log.error("\tLocal path '{}' does not exists. Pass...", s.getLocalPath());
                    continue;
                }
                HashMap<String, File> styles = new HashMap<>();
                // Single file
                if (localPath.isFile()) {
                    int pos = localPath.getName().lastIndexOf(".");
                    String styleName = pos > 0 ? localPath.getName().substring(0, pos) : localPath.getName();

                    if (s.getName() != null && !s.getName().isEmpty())
                        styleName = s.getName();
                    styles.put(styleName, localPath);
                }
                // Local dir
                else {
                    // TODO: auto mapping StyleType to file ext?
                    for(File f: localPath.listFiles()){
                        if(f.getName().endsWith(s.getFileExt())) {
                            int pos = f.getName().lastIndexOf(".");
                            String styleName = pos > 0 ? f.getName().substring(0, pos) : f.getName();
                            styles.put(styleName, f);
                        }
                    }
                }
                log.info(String.valueOf(styles));


                for(Map.Entry<String, File> styleInfo: styles.entrySet()) {
                    // Check style exists on server
                    if (this.geoserverClient.styleExists(w.getName(), styleInfo.getKey())) {
                        log.info("\tRemote style '{}.{}' already exists. Pass...", w.getName(), styleInfo.getKey());
                        continue;
                    }
                    // Try to upload
                    log.info("\tRemote style '{}.{}' does not exists. Upload...", w.getName(), styleInfo.getKey());
                    boolean ret = this.geoserverClient.uploadStyle(w.getName(), styleInfo.getKey(), styleInfo.getValue(), this.styleMimeTypes.get(s.getType()));
                    log.info(ret ? "\tOK" : "\tFail");
                }

            }
        }
        return true;
    }

}
