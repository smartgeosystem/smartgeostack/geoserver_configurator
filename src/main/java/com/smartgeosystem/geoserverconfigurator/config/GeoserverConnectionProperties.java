package com.smartgeosystem.geoserverconfigurator.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "geoserver", ignoreUnknownFields = true)
public class GeoserverConnectionProperties {
    private String baseAddress;
    private String user;
    private String password;
}
