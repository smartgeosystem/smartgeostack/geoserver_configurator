package com.smartgeosystem.geoserverconfigurator.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "configurator", ignoreUnknownFields = true)
public class ConfiguratorProperties {
    private boolean waitGeoserver;
    private int waitTimeout;
    private int waitPeriod;
}
