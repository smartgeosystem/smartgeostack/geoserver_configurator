package com.smartgeosystem.geoserverconfigurator.config;

import com.smartgeosystem.geoserverconfigurator.domain.DataFileHelper;
import com.smartgeosystem.geoserverconfigurator.domain.Workspace;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Data
@org.springframework.boot.context.properties.ConfigurationProperties(prefix = "configuration", ignoreUnknownFields = true)
public class GeoserverConfigurationProperties {
    private List<DataFileHelper> dataFiles;
    private List<Workspace> workspaces;
}
