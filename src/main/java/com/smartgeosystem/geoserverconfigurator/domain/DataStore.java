package com.smartgeosystem.geoserverconfigurator.domain;

import lombok.Data;

import java.util.ArrayList;


@Data
public class DataStore {
    private String name;
    private DataStoreType type;
    private Workspace workspace;

    //PG
    private String host="127.0.0.1";
    private int port=5432;
    private String user="postgres";
    private String passwd="";
    private String db="postgres";
    private String schema="public";

    //GPKG
    private String path;

    //Publish layers
    private ArrayList<String> publishLayers=new ArrayList<>();

    //Apply styles
    private boolean applySameNameStyles=false;
}
