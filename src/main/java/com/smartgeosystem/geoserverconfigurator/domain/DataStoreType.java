package com.smartgeosystem.geoserverconfigurator.domain;

public enum DataStoreType {
    PG,
    GPKG
}
