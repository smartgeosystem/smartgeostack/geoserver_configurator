package com.smartgeosystem.geoserverconfigurator.domain;

import lombok.Data;

@Data
public class StyleHelper {
    private StyleType type = StyleType.SLD_1_0;
    private String localPath;
    //Single file
    private String name;
    //Local dir
    private String fileExt = "sld";
}
