package com.smartgeosystem.geoserverconfigurator.domain;

public enum StyleType {
    SLD_1_0,
    SLD_1_1,
    SLD_ZIP,
    CSS
}
