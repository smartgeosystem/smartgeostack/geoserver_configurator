package com.smartgeosystem.geoserverconfigurator.domain;

import lombok.Data;

import java.util.ArrayList;

@Data
public class Workspace {
    private String name;
    private ArrayList<DataStore> dataStores=new ArrayList<>();
    private ArrayList<StyleHelper> styles=new ArrayList<>();


    public ArrayList<DataStore> getDataStores() {
        // TODO: move to properties converter?
        for(DataStore ds: this.dataStores) {
            ds.setWorkspace(this);
        }
        return this.dataStores;
    }
}
