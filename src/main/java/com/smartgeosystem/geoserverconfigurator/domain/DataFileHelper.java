package com.smartgeosystem.geoserverconfigurator.domain;

import lombok.Data;

@Data
public class DataFileHelper {
    private String localPath;
    private String remotePath;
}
