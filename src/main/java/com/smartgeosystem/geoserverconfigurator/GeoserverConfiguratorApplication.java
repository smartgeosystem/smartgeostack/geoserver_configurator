package com.smartgeosystem.geoserverconfigurator;

import com.smartgeosystem.geoserverconfigurator.config.ConfiguratorProperties;
import com.smartgeosystem.geoserverconfigurator.config.GeoserverConnectionProperties;
import com.smartgeosystem.geoserverconfigurator.config.GeoserverConfigurationProperties;
import com.smartgeosystem.geoserverconfigurator.service.GeoserverConfiguratorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Slf4j
@SpringBootApplication
@EnableConfigurationProperties({ConfiguratorProperties.class,
								GeoserverConnectionProperties.class,
								GeoserverConfigurationProperties.class})
public class GeoserverConfiguratorApplication implements CommandLineRunner {

	private GeoserverConfiguratorService geoserverConfigurator;

	public GeoserverConfiguratorApplication(GeoserverConfiguratorService geoserverConfigurator) {
		this.geoserverConfigurator = geoserverConfigurator;
	}

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(GeoserverConfiguratorApplication.class);
		app.run(args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("Start configurator...");
		log.info("Wait geoserver...");
		if (this.geoserverConfigurator.waitGeoserver())
			log.info("OK");
		else {
			log.error("Geoserver not started");
			log.info("Configurator finished...");
			return;
		}
		log.info("Upload data files...");
		this.geoserverConfigurator.uploadDataFiles();

		//log.info("Upload markers...");  //TODO

		log.info("Configure workspaces...");
		this.geoserverConfigurator.configureWorkspaces();
		log.info("Configure styles...");
		this.geoserverConfigurator.configureStyles();
		log.info("Configure datastores...");
		this.geoserverConfigurator.configureDataStores();
	}

}
